*** api User
+ get - delete - post - put 
http://localhost:4000/api/v1/users/

+ like restaurant
http://localhost:4000/api/v1/users/:userId/like
+ rate restaurant
http://localhost:4000/api/v1/users/:userId/rate
-body example:
{
  "restaurantId": 2
}

+ oder food
http://localhost:4000/api/v1/users/:userId/order
-body example:
[
  {
  "foodId": 3,
  "amount": 4,
  },
  {
  "foodId": 4,
  "amount": 5,
  }
]

+get Oder List
http://localhost:4000/api/v1/users/:userId/order_list/:orderId?



*** api Restaurant
+ get - delete - post - put 
http://localhost:4000/api/v1/restaurants/

