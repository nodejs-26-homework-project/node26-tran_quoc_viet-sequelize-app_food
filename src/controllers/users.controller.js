const response = require("../helpers/response");
const usersService = require("../services/users.service");

const usersController = {
  get: () => {
    return async (req, res, next) => {
      try {
        const { id } = req.params;

        const users = await usersService.get(id);
        res.status(200).json(response(users));
      } catch (error) {
        console.error("-------->: ", error);
        next(error);
      }
    };
  },

  likeRestaurant: () => {
    return async (req, res, next) => {
      try {
        const { userId } = req.params;
        const { restaurantId } = req.body;

        const likeOrUnlike = await usersService.likeRestaurant(
          userId,
          restaurantId
        );
        res.status(200).json(response(likeOrUnlike));
      } catch (error) {
        console.error("-------->: ", error);
        next(error);
      }
    };
  },

  rateRestaurant: () => {
    return async (req, res, next) => {
      try {
        const { userId } = req.params;
        const { restaurantId, amount } = req.body;

        const rateOrUnrate = await usersService.rateRestaurant(
          userId,
          restaurantId,
          amount
        );
        res.status(200).json(response(rateOrUnrate));
      } catch (error) {
        console.error("-------->: ", error);
        next(error);
      }
    };
  },

  oderFood: () => {
    return async (req, res, next) => {
      try {
        const { userId } = req.params;
        const oderData = req.body;

        const oder = await usersService.oderFood(userId, oderData);
        res.status(200).json(response(oder));
      } catch (error) {
        console.error("-------->: ", error);
        next(error);
      }
    };
  },

  getOderList: () => {
    return async (req, res, next) => {
      try {
        const { userId, orderId } = req.params;

        const userWithOderList = await usersService.getOderList(userId, orderId);
        res.status(200).json(response(userWithOderList));
      } catch (error) {
        console.error("-------->: ", error);
        next(error);
      }
    };
  },
};

module.exports = usersController;
