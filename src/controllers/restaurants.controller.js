const response = require("../helpers/response");
const restaurantsService = require("../services/restaurants.service");

const restaurantsController = {
  get: () => {
    return async (req, res, next) => {
      try {
        const { id } = req.params;

        const restaurants = await restaurantsService.get(id);
        res.status(200).json(response(restaurants));
      } catch (error) {
        console.error("-------->: ", error);
        next(error);
      }
    };
  },
};

module.exports = restaurantsController;
