const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "Food",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
      },
      restaurantId: {
        type: DataTypes.INTEGER,
        // allowNull: false,
        field: "restaurant_id",
      },
      price: {
        type: DataTypes.FLOAT,
      },
    },
    {
      tableName: "food",
      timestamps: false,
    }
  );
};
