const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("sequelize_app_food", "root", "123", {
  dialect: "mysql",
  host: "localhost",
  port: 3306,
});

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Sequelize connected okk");
  } catch (error) {
    console.log("Something wrong with Sequelizzze");
  }
})();

//Initiate Main Models
const User = require("../models/mainModels/User")(sequelize);
const Restaurant = require("../models/mainModels/Restaurant")(sequelize);
const Food = require("../models/mainModels/Food")(sequelize);
const Oder = require("../models/mainModels/Oder")(sequelize);

//Initiate Relation Models
const LikeRestaurant = require("../models/relationModels/LikeRestaurant")(sequelize);
const RateRestaurant = require("./relationModels/RateRestaurant")(sequelize);
const OderDetail = require("./relationModels/OderDetail")(sequelize);

//Create Relations
//User has Restaurants
User.hasMany(Restaurant, { as: "ownRestaurants", foreignKey: "userId" });
Restaurant.belongsTo(User, { as: "owner", foreignKey: "userId" });

//User has Oders
User.hasMany(Oder, { as: "hasOders", foreignKey: "userId" });
Oder.belongsTo(User, { as: "fromUser", foreignKey: "userId" });

//Users like Restaurants
User.belongsToMany(Restaurant, {
  as: "likeRestaurants",
  through: LikeRestaurant,
  foreignKey: "userId",
});
Restaurant.belongsToMany(User, {
  as: "userLikes",
  through: LikeRestaurant,
  foreignKey: "restaurantId",
});

//Users rate Restaurants
User.belongsToMany(Restaurant, {
  as: "rateRestaurants",
  through: RateRestaurant,
  foreignKey: "userId",
});
Restaurant.belongsToMany(User, {
  as: "userRates",
  through: RateRestaurant,
  foreignKey: "restaurantId",
});

//Oder Detail
Oder.belongsToMany(Food, {
  as: "food",
  through: OderDetail,
  foreignKey: "oderId",
});
Food.belongsToMany(Oder, {
  as: "inOders",
  through: OderDetail,
  foreignKey: "foodId",
});


module.exports = {
  sequelize,
  User,
  Restaurant,
  LikeRestaurant,
  RateRestaurant,
  Food,
  Oder,
  OderDetail,
};
