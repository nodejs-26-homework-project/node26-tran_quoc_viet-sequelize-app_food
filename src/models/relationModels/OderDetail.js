const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "OderDetail",
    {
      oderId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: "oder_id",
      },
      foodId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: "food_id",
      },
      amount: {
        type: DataTypes.INTEGER,
      },
      price: {
        type: DataTypes.FLOAT,
      },
    },
    {
      tableName: "oder_details",
      timestamps: false,
    }
  );
};
