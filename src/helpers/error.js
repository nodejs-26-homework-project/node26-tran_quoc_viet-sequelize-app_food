class AppError extends Error {
  constructor(statusCode, message) {
    super(message);
    this.statusCode = statusCode;
  }
}

const handleError = (err, req, res, next) => {
  if (!(err instanceof AppError)) {
    err = new AppError(500, "Internal Serverrr");
  }

  const { message, statusCode } = err;
  res.status(statusCode).json({
    status: "Error",
    message,
  });

  next();
};

module.exports = { handleError, AppError };
