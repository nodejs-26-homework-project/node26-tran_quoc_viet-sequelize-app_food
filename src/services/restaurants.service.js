const { AppError } = require("../helpers/error");
const { Restaurant } = require("../models");

const restaurantsService = {
  get: async (restaurantId) => {
    try {
      if (restaurantId) {
        const restaurant = await Restaurant.findByPk(restaurantId, {
          include: [
            {
              association: "owner",
              attributes: {
                exclude: ["id", "password"],
              },
            },
            {
              association: "userLikes",
              through: {
                attributes: [],
              },
            },
            {
              association: "userRates",
              through: {
                attributes: [],
              },
            },
          ],
          attributes: {
            exclude: ["userId"],
          },
        });
        if (restaurant) {
          return restaurant;
        }
        throw new AppError(400, "Restaurant not found");
      }

      const restaurants = await Restaurant.findAll({
        include: [
          {
            association: "owner",
            attributes: {
              exclude: ["id", "password"],
            },
          },
          {
            association: "userLikes",
            through: {
              attributes: [],
            },
          },
          {
            association: "userRates",
            through: {
              attributes: [],
            },
          },
        ],
        attributes: {
          exclude: ["userId"],
        },
      });
      return restaurants;
    } catch (error) {
      throw error;
    }
  },
};

module.exports = restaurantsService;
