const { AppError } = require("../helpers/error");
const {
  User,
  Restaurant,
  RateRestaurant,
  Food,
  Oder,
  OderDetail,
} = require("../models");

const usersService = {
  get: async (userId) => {
    try {
      if (userId) {
        const user = await User.findByPk(userId, {
          include: [
            {
              association: "ownRestaurants",
              attributes: {
                exclude: ["userId"],
              },
            },
            {
              association: "likeRestaurants",
              through: {
                attributes: [],
              },
              attributes: {
                exclude: ["userId"],
              },
            },
            {
              association: "rateRestaurants",
              through: {
                attributes: [],
              },
              attributes: {
                exclude: ["userId"],
              },
            },
          ],
        });
        if (user) {
          return user;
        }
        throw new AppError(400, "User not found");
      }

      const users = await User.findAll({
        include: [
          {
            association: "ownRestaurants",
            attributes: {
              exclude: ["userId"],
            },
          },
          {
            association: "likeRestaurants",
            through: {
              attributes: [],
            },
            attributes: {
              exclude: ["userId"],
            },
          },
          {
            association: "rateRestaurants",
            through: {
              attributes: [],
            },
            attributes: {
              exclude: ["userId"],
            },
          },
        ],
      });
      return users;
    } catch (error) {
      throw error;
    }
  },

  likeRestaurant: async (userId, restaurantId) => {
    try {
      const user = await User.findByPk(userId);
      if (!user) {
        throw new AppError(400, "User not found");
      }

      const restaurant = await Restaurant.findByPk(restaurantId);
      if (!restaurant) {
        throw new AppError(400, "Restaurant not found");
      }

      // console.log(user.__proto__);
      const hasLike = await user.hasLikeRestaurant(restaurantId);
      if (hasLike) {
        await user.removeLikeRestaurant(restaurantId);
        return "Unliked";
      } else {
        await user.addLikeRestaurant(restaurantId);
        return "Liked";
      }
    } catch (error) {
      throw error;
    }
  },

  rateRestaurant: async (userId, restaurantId, rateAmount) => {
    try {
      const user = await User.findByPk(userId);
      if (!user) {
        throw new AppError(400, "User not found");
      }

      const restaurant = await Restaurant.findByPk(restaurantId);
      if (!restaurant) {
        throw new AppError(400, "Restaurant not found");
      }

      // console.log(user.__proto__);
      const hasRate = await user.hasRateRestaurant(restaurantId);
      if (hasRate) {
        await user.removeRateRestaurant(restaurantId);
        return "Unrated";
      } else {
        await RateRestaurant.create({
          userId,
          restaurantId,
          amount: rateAmount,
        });
        return `Rated ${rateAmount}`;
      }
    } catch (error) {
      throw error;
    }
  },

  oderFood: async (userId, oderData) => {
    try {
      const user = await User.findByPk(userId);
      if (!user) {
        throw new AppError(400, "User not found");
      }
      if (!oderData.length) {
        throw new AppError(400, "Please post a valid data format");
      }

      let foodList = [];
      for (let index = 0; index < oderData.length; index++) {
        let food = await Food.findByPk(oderData[index].foodId);
        if (!food) {
          throw new AppError(
            400,
            `the food with id-${oderData[index].foodId} unavailable`
          );
        }
        if (oderData[index].amount < 1) {
          throw new AppError(
            400,
            `the amount of food with id-${oderData[index].foodId} must be larger than 0`
          );
        }
        foodList.push(food);
      }

      const oder = await Oder.create({ userId });

      for (let index = 0; index < foodList.length; index++) {
        await OderDetail.create({
          oderId: oder.id,
          foodId: foodList[index].id,
          amount: oderData[index].amount,
          price: foodList[index].price * oderData[index].amount,
        });
      }

      let totalPrice = await OderDetail.sum("price", {
        where: {
          oderId: oder.id,
        },
      });

      await Oder.update(
        { totalPrice: totalPrice },
        {
          where: {
            id: oder.id,
          },
        }
      );

      let oderDetail = await Oder.findByPk(oder.id, {
        include: [
          {
            association: "food",
            through: {
              as: "oderDetail",
              attributes: {
                exclude: ["oderId", "foodId"],
              },
            },
          },
        ],
      });

      return oderDetail;
    } catch (error) {
      throw error;
    }
  },

  getOderList: async (userId, orderId) => {
    try {
      if (orderId) {
        const oder = await Oder.findByPk(orderId);
        if (!oder) {
          throw new AppError(400, `Cannot find order with id-${orderId}`);
        }

        const userWithOneOrder = await User.findByPk(userId, {
          include: [
            {
              association: "hasOders",
              attributes: {
                exclude: ["userId"],
              },
              include: [
                {
                  association: "food",
                  through: {
                    as: "orderDetail",
                    attributes: {
                      exclude: ["oderId", "foodId"],
                    },
                  },
                },
              ],
              where: {
                id: orderId,
              },
            },
          ],
        });
        if (userWithOneOrder) {
          return userWithOneOrder;
        }
        throw new AppError(
          400,
          `User not found or maybe this User has not placed an order with id-${orderId}`
        );
      }

      const userWithOderList = await User.findByPk(userId, {
        include: [
          {
            association: "hasOders",
            attributes: {
              exclude: ["userId"],
            },
            include: [
              {
                association: "food",
                through: {
                  as: "orderDetail",
                  attributes: {
                    exclude: ["oderId", "foodId"],
                  },
                },
              },
            ],
          },
        ],
      });
      if (userWithOderList) {
        return userWithOderList;
      }
      throw new AppError(400, "User not found");
    } catch (error) {
      throw error;
    }
  },
};

module.exports = usersService;
