const express = require("express");
const { handleError } = require("./helpers/error");
const { sequelize } = require("./models");
const app = express();
app.use(express.json());

sequelize.sync({ alter: true });

const v1 = require("./routers/v1");

app.use("/api/v1", v1);

app.use(handleError);

app.listen(4000);
