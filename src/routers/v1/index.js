const express = require('express');
const restaurantsRouters = require('./restaurants.router');
const usersRouters = require('./users.routers');

const v1 = express.Router();

v1.use('/users', usersRouters);
v1.use('/restaurants', restaurantsRouters);

module.exports = v1;