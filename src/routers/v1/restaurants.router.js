const express = require("express");
const restaurantsController = require("../../controllers/restaurants.controller");

const restaurantsRouters = express.Router();

restaurantsRouters.get("/:id?", restaurantsController.get());

module.exports = restaurantsRouters;
