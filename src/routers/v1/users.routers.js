const express = require("express");
const usersController = require("../../controllers/users.controller");

const usersRouters = express.Router();

usersRouters.get("/:id?", usersController.get());
usersRouters.post("/:userId/like", usersController.likeRestaurant());
usersRouters.post("/:userId/rate", usersController.rateRestaurant());
usersRouters.post("/:userId/order", usersController.oderFood());
usersRouters.get("/:userId/order_list/:orderId?", usersController.getOderList());

module.exports = usersRouters;
