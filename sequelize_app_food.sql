-- sequelize_app_food - Node26 Trần Quốc Việt
-- Query để tạo dữ liệu test

-- Tạo và sử dụng database
DROP DATABASE if EXISTS sequelize_app_food;
CREATE DATABASE sequelize_app_food;
USE sequelize_app_food;


DELETE FROM users;
ALTER TABLE users AUTO_INCREMENT = 1;

INSERT INTO users(full_name, email, password)
VALUES
   ('Tran Van A', 'tva@g.co', 'a123'),
   ('Tran Van B', 'tvb@g.co', 'a123'),
   ('Tran Van C', 'tvc@g.co', 'a123'),
   ('Tran Van D', 'tvd@g.co', 'a123'),
   ('Tran Van E', 'tve@g.co', 'a123'),
   ('Tran Van F', 'tvf@g.co', 'a123'),
   ('Tran Van G', 'tvg@g.co', 'a123'),
   ('Tran Van H', 'tvh@g.co', 'a123'),
   ('Tran Van I', 'tvi@g.co', 'a123'),
   ('Tran Van G', 'tvj@g.co', 'a123'),
   ('Tran Van K', 'tvk@g.co', 'a123'),
   ('Tran Van L', 'tvl@g.co', 'a123'),
   ('Tran Van M', 'tvm@g.co', 'a123'),
   ('Tran Van N', 'tvn@g.co', 'a123'),
   ('Tran Van O', 'tvo@g.co', 'a123'),
   ('Tran Van P', 'tvp@g.co', 'a123'),
   ('Tran Van Q', 'tvq@g.co', 'a123'),
   ('Tran Van R', 'tvr@g.co', 'a123'),
   ('Tran Van S', 'tvs@g.co', 'a123'),
   ('Tran Van T', 'tvt@g.co', 'a123') ;


-- Table restaurants

DELETE FROM restaurants;
ALTER TABLE restaurants AUTO_INCREMENT = 1;

INSERT INTO restaurants(name, image, description, user_id)
VALUES
   ('KFC', 'https://static.kfcvietnam.com.vn/images/category/lg/COMBO%201%20NGUOI.jpg?v=E3QQZ3', 'Kentucky Fried Chicken', 2),
   ('Lotteria', 'http://smart-supermarket.com.vn/wp-content/uploads/2020/06/a5-e1593482773289.jpg', 'Fried Chicken from Korea', 4),
   ('Burger King', 'https://images.foody.vn/res/g3/26916/prof/s640x400/foody-upload-api-foody-mobile-sh-b511f740-221001081535.jpeg', 'King of Hamburger', 4),
   ('MAYCHA', 'https://images.foody.vn/res/g74/739438/prof/s1242x600/foody-upload-api-foody-mobile-2a-200504093905.jpg', 'Tà tữa của May', 7),
   ('KOI', 'https://aeonmall-hadong.com.vn/wp-content/uploads/2020/05/bubble-milk-tea.jpg', 'Koi tà tữa thử Koi', 6)  ;


-- Table food type

DELETE FROM food_types;
ALTER TABLE food_types AUTO_INCREMENT = 1;

INSERT INTO food_types(name)
VALUES
   ('Fried chicken'),
   ('Burger'),
   ('Fried potato'),
   ('Tea'),
   ('Milk'),
   ('Ice cream')  ;


-- Table food

DELETE FROM food;
ALTER TABLE food auto_increment = 1;

INSERT INTO food(name, image, price, description, type_id)
VALUES
   ('Fried Chicken 1', 'https://static.kfcvietnam.com.vn/images/category/lg/COMBO%201%20NGUOI.jpg?v=E3QQZ3', 77000, 'Fresh chicken wings', 1 ),
   ('Fried Chicken 2', 'https://c.ndtvimg.com/2020-08/2dv9fku_kfc-covid_625x300_25_August_20.jpg', 65000, 'Fresh chicken legs', 1 ),
   ('Hamburger 1', 'https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg', 53000, 'Burger with beef', 2 ),
   ('Hamburger 2', 'https://images.bolt.eu/store/2022/2022-09-01/c2098142-363c-48d9-a541-51586e14b703.jpeg', 46000, 'Burger with pork', 2 ),
   ('French Fried', 'https://i.insider.com/564a5db0112314303e8b577d?width=1136&format=jpeg', 33000, 'Fresh potato', 3 ),
   ('Bubble Tea', 'https://cdn.vox-cdn.com/thumbor/BLEQSyZC9voUX7wpIkAiZiETAy0=/0x0:3625x3625/1200x0/filters:focal(0x0:3625x3625):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/14548425/SML_190218_1054.jpg', 69000, 'Bubble tea Hong Kong style', 4 ),
   ('Milk With Boba', 'http://gourmetvegetariankitchen.com/wp-content/uploads/2018/04/bubble-tea-without-tea.jpg', 66000, 'Fresh milk and black sugar boba', 5 ),
   ('Ice Cream', 'https://www.koshericecream.com/wp-content/uploads/van-choc-swirl.png', 5000, 'Fresh milk ice cream', 6 )
   ;

-- Table sub_food

DELETE FROM sub_food;
ALTER TABLE sub_food AUTO_INCREMENT = 1;

INSERT INTO sub_food(name, price, food_id)
VALUES
   ('Tomato', 3000, 1),
   ('Salad', 4000, 1),
   ('Pepsi', 2000, 2),
   ('Coke', 2500, 2),
   ('7up', 3000, 2),
   ('Fresh Water', 1000, 3),
   ('Butter', 1500, 3),
   ('Mayonaise', 700, 3),
   ('Cucumber', 3000, 4),
   ('Ketchup', 500, 5),
   ('Jello', 3000, 6),
   ('Flan', 3500, 6),
   ('Boba', 2000, 6),
   ('Sugar', 400, 6),
   ('Non-fat Milk', 6000, 7),
   ('Soya Milk', 5500, 7),
   ('Chocolate Chip', 800, 8),
   ('Haribo', 900, 8),
   ('Condensed Milk', 600, 8)   ;


-- Table orders

DELETE FROM orders;

INSERT INTO orders(user_id, food_id, amount, arr_sub_id, code, date_order)
VALUES
   (1, 2, 1, '3, 4', '', '2022-02-02 22-22-22'),
   (4, 5, 4, '10', '', '2022-02-03 20-24-52'),
   (6, 5, 2, '10', 'DISCOUNT20', '2022-02-04 12-00-24'),
   (11, 1, 3, '1, 2', '', '2022-02-06 09-21-15'),
   (16, 8, 5, '17, 19', '', '2022-02-06 10-22-42'),
   (7, 6, 2, '12, 13', 'DISCOUNT40', '2022-02-06 13-44-53'),
   (3, 1, 4, '2', 'FREESHIP', '2022-02-07 05-25-32'),
   (12, 3, 2, '6', '', '2022-02-07 11-32-09'),
   (19, 7, 1, '16', '', '2022-02-08 08-28-52'),
   (17, 7, 6, '15, 16', 'FREESHIP', '2022-02-08 08-29-29'),
   (3, 8, 5, '19', '', '2022-02-08 09-24-33'),
   (5, 4, 2, '9', '', '2022-02-08 10-55-21'),
   (13, 7, 14, '15', 'DISCOUNT40', '2022-02-08 11-11-47'),
   (20, 1, 8, '2', '', '2022-02-08 12-12-31'),
   (15, 2, 5, '4', 'FREESHIP', '2022-02-08 14-52-52'),
   (4, 2, 2, '5', '', '2022-02-08 17-09-28'),
   (12, 8, 4, '18', 'DISCOUNT10', '2022-02-08 19-08-04'),
   (13, 6, 2, '13', '', '2022-02-09 09-08-07'),
   (1, 2, 3, '5', 'FREESHIP', '2022-02-10 17-29-42'),
   (13, 6, 5, '13', '', '2022-02-11 11-08-11')
   ;
   

-- Table rate_restaurants

DELETE FROM rate_restaurants;

INSERT INTO rate_restaurants(user_id, restaurant_id, amount, created_at)
VALUES
   (1, 1, 4, '2022-02-02 22-22-22'),
   (4, 1, 3, '2022-02-03 20-24-52'),
   (6, 3, 5, '2022-02-04 12-00-24'),
   (11, 1, 3, '2022-02-06 09-21-15'),
   (16, 2, 5, '2022-02-06 10-22-42'),
   (7, 5, 3, '2022-02-06 13-44-53'),
   (3, 1, 1, '2022-02-07 05-25-32'),
   (12, 3, 5, '2022-02-07 11-32-09'),
   (19, 4, 4, '2022-02-08 08-28-52'),
   (17, 4, 1, '2022-02-08 08-29-29'),
   (3, 2, 5, '2022-02-08 09-24-33'),
   (5, 3, 2, '2022-02-08 10-55-21'),
   (13, 4, 1, '2022-02-08 11-11-47'),
   (20, 1, 1, '2022-02-08 12-12-31'),
   (15, 2, 5, '2022-02-08 14-52-52'),
   (4, 2, 2, '2022-02-08 17-09-28'),
   (12, 4, 4, '2022-02-08 19-08-04'),
   (13, 5, 2, '2022-02-09 09-08-07'),
   (1, 2, 5, '2022-02-10 17-29-42')
   ;


-- Table like_restaurants

DELETE FROM like_restaurants;

INSERT INTO like_restaurants(user_id, restaurant_id)
VALUES
   (5, 2),
   (3, 4),
   (9, 4),
   (8, 2),
   (8, 1),
   (8, 5),
   (11, 3),
   (1, 2),
   (16, 2),
   (12, 4),
   (3, 2)  ;


